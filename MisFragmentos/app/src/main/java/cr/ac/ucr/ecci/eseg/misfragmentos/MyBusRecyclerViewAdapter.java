package cr.ac.ucr.ecci.eseg.misfragmentos;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
/**
 * {@link RecyclerView.Adapter} that can display a {@link Bus}.
 */
public class MyBusRecyclerViewAdapter extends RecyclerView.Adapter<MyBusRecyclerViewAdapter.ViewHolder> {
    private final List<Bus> mValues;
    public Bus mItem;

    public MyBusRecyclerViewAdapter(List<Bus> items) {
        mValues = items;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getId());
// holder.mContentView.setText(mValues.get(position).getCapacidadText());
    }
    @Override
    public int getItemCount()
    {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public Bus mItem;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);

            view.setOnClickListener(new View.OnClickListener() {

                /*
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), mItem.getId(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mView.getContext(), DetailsActivity.class);
                    intent.putExtra("bus", mItem);
                    mView.getContext().startActivity(intent);
                }
                */

                @Override
                public void onClick(View view) {
                    ((MainActivity) view.getContext()).showDetail(mItem);
                }

            });
        }


        @Override
        public String toString()
        {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}

