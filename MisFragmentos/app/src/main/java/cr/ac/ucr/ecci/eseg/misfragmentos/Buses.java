package cr.ac.ucr.ecci.eseg.misfragmentos;
import java.util.ArrayList;
import java.util.List;
public final class Buses {
    public static final List<Bus> buses = new ArrayList<Bus>(){
        {
            add(new Bus("UCR01", 80, "Aire Acondicionado"));
            add(new Bus("UCR02", 70, "Motor de baja potencia"));
            add(new Bus("UCR03", 60, "Sistema de audio"));
        }
    };
}