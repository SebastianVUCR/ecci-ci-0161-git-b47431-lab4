package cr.ac.ucr.ecci.eseg.misfragmentos;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment {
    // the fragment initialization parameters
    private static final String ARG_BUS = "bus";
    private Bus bus;
    public DetailsFragment() {
// Required empty public constructor
    }
    public DetailsFragment(Bus bus) {
        this.bus = bus;
    }
    public static DetailsFragment newInstance(Bus bus) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_BUS, bus);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bus = getArguments().getParcelable(ARG_BUS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
// Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_details, container, false);
        if(bus!=null) {
            TextView textID = (TextView) v.findViewById(R.id.textID);
            textID.setText(bus.getId());
            TextView textCapacity = (TextView) v.findViewById(R.id.textCapacity);
            textCapacity.setText(bus.getCapacidadText());
            TextView textDetails = (TextView) v.findViewById(R.id.textDetails);
            textDetails.setText(bus.getDescripcion());

        }
        return v;
    }
}