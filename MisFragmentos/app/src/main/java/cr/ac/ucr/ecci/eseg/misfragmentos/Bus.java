package cr.ac.ucr.ecci.eseg.misfragmentos;
import android.os.Parcel;
import android.os.Parcelable;
public class Bus implements Parcelable {
    private String id;
    private int capacidad;
    private String descripcion;
    public Bus() {
    }
    public Bus(String id, int capacidad, String descripcion) {
        this.id = id;
        this.capacidad = capacidad;
        this.descripcion = descripcion;
    }
    private Bus(Parcel in) {
        this.id = in.readString();
        this.capacidad = in.readInt();
        this.descripcion = in.readString();
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public int getCapacidad() {
        return capacidad;
    }
    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }
    public String getCapacidadText() {
        return getCapacidad() + " pasajeros";
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel out, int falgs) {
        out.writeString(getId());
        out.writeInt(getCapacidad());
        out.writeString(getDescripcion());
    }
    public static final Creator<Bus> CREATOR =
            new Creator<Bus>() {
                public Bus createFromParcel(Parcel in) {
                    return new Bus(in);
                }
                @Override
                public Bus[] newArray(int size) {
                    return new Bus[size];
                }
            };
}